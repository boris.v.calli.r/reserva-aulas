
let submitFormIndividual = document.getElementById('crear-ambiente');

submitFormIndividual.addEventListener('submit', async (e) => {
  e.preventDefault();
  let data = {};
  const formData = new FormData(submitFormIndividual);
  formData.forEach((v, k) => {
    if (k != 'facilidades') {
      data[k] = v;
    } else {
      data[k] = formData.getAll(k);
    }
  })
  try {
    let res = await fetch('/api/ambiente', {
      method: 'post',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
    let status = res.status;
    if (res.ok && status == 200) {
      alert("Se creo un nuevo ambiente");
    }
  } catch (error) {
    alert(error);
  }
})