
let reservarForm = document.getElementById('reservar-form');
document.addEventListener('DOMContentLoaded', () => {
  let aulasElem = document.getElementById("aulas");
  let capacidadLabelElem = document.getElementById('capacidad')
  let hora_ini = document.getElementById('hora-ini');
  let hora_fin = document.getElementById('hora-fin');
  let filtroBtn = document.getElementById('filtro')
  let minCapElem = document.getElementById('min-capacidad');

  setAulasData(aulas);
  aulasCapacidadListeners(aulasElem, capacidadLabelElem);
  horasListeners(hora_ini, hora_fin);


  filtroBtn.addEventListener('click', () => {
    let minCap = minCapElem.value;
    capacidadLabelElem.innerText = "";
    let aulasFiltradas = aulas.filter(aula => aula.capacidad >= minCap);
    let str = '<option value="">Aulas:</option>';
    aulasFiltradas.forEach(aula => {
      let html = `<option value='${aula.ambiente}'>${aula.ambiente}</option>`
      str += html;
    });
    aulasElem.innerHTML = str;
  });




});





function horaASegundos(hora) {
  let hh = hora.split(/:/);
  let seconds = 0;
  seconds += (3600 * parseInt(hh[0]));
  seconds += (60 * parseInt(hh[1]));
  return seconds;
}
function horasListeners(hora_ini, hora_fin) {
  hora_ini.addEventListener('change', () => {
    let horaVal = horaASegundos(hora_ini.value);
    hora_fin.selectedIndex = 0;
    for (let option of hora_fin.options) {
      if (option.selected) {
        option.value = "";
      }
      let horaVal2 = horaASegundos(option.value);
      option.disabled = (horaVal >= horaVal2);
    }
  });
}
function setAulasData(aulas) {
  localStorage.clear();
  aulas.forEach(e => {
    localStorage.setItem(e.ambiente, e.capacidad);
  });
}
function aulasCapacidadListeners(aulasElem, capacidadLabelElem) {
  capacidadLabelElem.innerText = localStorage.getItem(aulasElem.value);
  aulasElem.addEventListener('change', (e) => {
    let val = aulasElem.value;
    capacidadLabelElem.innerText = localStorage.getItem(val);
  });
}