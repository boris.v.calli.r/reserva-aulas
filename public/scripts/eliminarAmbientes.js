let tablaActivas = document.getElementById('tabla-activas')
let tablaDesactivas = document.getElementById('tabla-desactivas')

document.addEventListener('DOMContentLoaded', () => {

  setEliminarEventHandlers();
  setActivarEventHandlers();
});

function setEliminarEventHandlers() {
  tablaActivas.addEventListener('click', e => {
    if (e.target.tagName === "A") {
      eliminarAula(e);
    }
  });
}
function setActivarEventHandlers() {
  tablaDesactivas.addEventListener('click', e => {
    if (e.target.tagName === "A") {
      activarAula(e);
    }
  });
}

async function activarAula(e) {
  let aulaId = e.target.closest('tr').dataset.id;
  await fetch(`http://localhost:3000/api/ambiente/${aulaId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  await renderTablaAulasDesactivas();
  await renderTablaAulasActivas();
}
async function eliminarAula(e) {
  let aulaId = e.target.closest('tr').dataset.id;
  await fetch(`http://localhost:3000/api/ambiente/${aulaId}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json'
    }
  });
  await renderTablaAulasDesactivas();
  await renderTablaAulasActivas();
}

function eliminarFilas(elem) {
  while (elem.firstChild) {
    elem.removeChild(elem.firstChild);
  }
}

async function renderTablaAulasActivas() {
  let aulas = await (await fetch(`http://localhost:3000/api/ambiente`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })).json();
  let dataAula = document.getElementById('tabla-content-activas')
  eliminarFilas(dataAula);
  let aulasActivas = aulas.filter(aula => aula.activo == true);
  aulasActivas.forEach(aula => {
    let nodoAula = document.createElement('tr');
    nodoAula.dataset.id = aula.id;
    dataAula.appendChild(nodoAula);
    let nodoCol1 = document.createElement('td');
    nodoCol1.innerText = `${aula.nombreAmbiente}`;
    nodoAula.appendChild(nodoCol1)
    let nodoCol2 = document.createElement('td');
    nodoCol2.innerText = `${aula.capacidad}`;
    nodoAula.appendChild(nodoCol2)
    let nodoCol3 = document.createElement('td');
    nodoCol3.innerText = `${aula.descripcion}`;
    nodoAula.appendChild(nodoCol3)
    let nodoCol4 = document.createElement('td');
    nodoCol4.innerText = `${aula.nombreTipo}`;
    nodoAula.appendChild(nodoCol4)
    let nodoCol5 = document.createElement('td');
    nodoCol5.innerText = `${aula.activo}`;
    nodoAula.appendChild(nodoCol5)
    let nodoCol6 = document.createElement('td');
    let aa = document.createElement('a');
    aa.classList.add('eliminar');
    aa.innerText = "desactivar"
    nodoCol6.appendChild(aa);
    nodoAula.appendChild(nodoCol6);
  });
}
async function renderTablaAulasDesactivas() {
  let aulas = await (await fetch(`http://localhost:3000/api/ambiente`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })).json();
  let dataAula = document.getElementById('tabla-content-desactivas')
  eliminarFilas(dataAula);
  let aulasActivas = aulas.filter(aula => aula.activo == false);
  aulasActivas.forEach(aula => {
    let nodoAula = document.createElement('tr');
    nodoAula.dataset.id = aula.id;
    dataAula.appendChild(nodoAula);
    let nodoCol1 = document.createElement('td');
    nodoCol1.innerText = `${aula.nombreAmbiente}`;
    nodoAula.appendChild(nodoCol1)
    let nodoCol2 = document.createElement('td');
    nodoCol2.innerText = `${aula.capacidad}`;
    nodoAula.appendChild(nodoCol2)
    let nodoCol3 = document.createElement('td');
    nodoCol3.innerText = `${aula.descripcion}`;
    nodoAula.appendChild(nodoCol3)
    let nodoCol4 = document.createElement('td');
    nodoCol4.innerText = `${aula.nombreTipo}`;
    nodoAula.appendChild(nodoCol4)
    let nodoCol5 = document.createElement('td');
    nodoCol5.innerText = `${aula.activo}`;
    nodoAula.appendChild(nodoCol5)
    let nodoCol6 = document.createElement('td');
    let aa = document.createElement('a');
    aa.classList.add('eliminar');
    aa.innerText = "activar"
    nodoCol6.appendChild(aa);
    nodoAula.appendChild(nodoCol6);
  });
}