import { sequelize } from '../database/db.js';
import { DataTypes } from 'sequelize';
import { Docente } from '../models/Docente.model.js';
import { Periodo } from '../models/Periodo.model.js';
import { Ambiente } from '../models/Ambiente.model.js';

//foreign keys
// docenteId ambienteId periodoIniId periodoFinId
export const Reserva = sequelize.define('Reseva', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  materia: {
    type: DataTypes.STRING,
    allowNull: false
  },
  razon: {
    type: DataTypes.STRING,
    allowNull: true
  },
  fecha: {
    type: DataTypes.DATE,
    allowNull: false
  }
}, {
  timestamps: false,
  tableName: 'Reservas',
  freezeTableName: true
})

Reserva.belongsTo(Ambiente, {
  foreignKey: "ambienteId",
  targetId: "id"
});
Ambiente.hasMany(Reserva, {
  foreignKey: "ambienteId",
  sourceKey: "id"
})


Reserva.belongsTo(Docente, {
  foreignKey: "docenteId",
  targetId: "id"
});
Docente.hasMany(Reserva, {
  foreignKey: "docenteId",
  sourceKey: "id"
});

Reserva.belongsTo(Periodo, {
  as: "periodoIni",
});
Reserva.belongsTo(Periodo, {
  as: "periodoFin",
});
// Periodo.hasMany(Reserva);