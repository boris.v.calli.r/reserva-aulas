import { sequelize } from '../database/db.js';
import { DataTypes } from "sequelize";
import { Facilidad } from './Facilidad.model.js';
import { Ambiente } from './Ambiente.model.js';

export const FacilidadAmbiente = sequelize.define('FacilidadAmbiente', {
    ambienteId: {
        type: DataTypes.INTEGER,
        references: {
            model: Ambiente,
            key: 'id'
        }
    },
    facilidadId: {
        type: DataTypes.INTEGER,
        references: {
            model: Facilidad,
            key: 'id'
        }
    }
}, {
    timestamps: false,
    tableName: 'FacilidadAmbiente',
    freezeTableName: true
});


Ambiente.belongsToMany(Facilidad, {
  through: FacilidadAmbiente,
  foreignKey:'ambienteId'
});
Facilidad.belongsToMany(Ambiente, {
  through: FacilidadAmbiente,
  foreignKey:'facilidadId'
});
