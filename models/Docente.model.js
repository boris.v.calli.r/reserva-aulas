import { sequelize } from '../database/db.js';
import { DataTypes } from 'sequelize';


//foreign keys
export const Docente = sequelize.define('Docente',{
  id: {
     type: DataTypes.INTEGER,
     autoIncrement:true,
     primaryKey:true 
  },
  nombre: {
    type: DataTypes.STRING,
    allowNull:false,
  },
  apellido:{
    type: DataTypes.STRING,
    allowNull:false
  },
  edad:{
    type:DataTypes.INTEGER,
    allowNull:false
  }
},{
  timestamps:false,
  tableName:'Docentes',
  freezeTableName:true
})
