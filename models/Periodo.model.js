import { sequelize } from '../database/db.js';
import { DataTypes } from 'sequelize';


//foreign keys
export const Periodo = sequelize.define('Periodo',{
  id: {
     type: DataTypes.INTEGER,
     autoIncrement:true,
     primaryKey:true 
  },
  hora: {
    type: DataTypes.STRING,
    allowNull:false
  }
},{
  timestamps:false,
  tableName:'Periodos',
  freezeTableName:true
})
