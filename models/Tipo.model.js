import { sequelize } from '../database/db.js';
import { DataTypes } from "sequelize";

export const Tipo = sequelize.define('Tipo', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    nombreTipo: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
    tableName: 'Tipos',
    freezeTableName: true
});