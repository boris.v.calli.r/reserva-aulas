import { sequelize } from "../database/db.js";
import { DataTypes } from "sequelize";
import { Tipo } from "../models/Tipo.model.js";

export const Ambiente = sequelize.define(
  "Ambiente",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    nombreAmbiente: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    descripcion: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    capacidad: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    activo: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    disponibilidad: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true,
    },
  },
  {
    timestamps: false,
    tableName: "Ambientes",
    freezeTableName: true,
  }
);

// Muchos ambientes pueden ser de un tipo
// Un tipo de ambiente puede pertenecer a muchos ambientes

Tipo.hasMany(Ambiente, {
  foreignKey: "tipoId"
});
Ambiente.belongsTo(Tipo, {
  foreignKey: "tipoId"
});
