import { sequelize } from '../database/db.js';
import { DataTypes } from "sequelize";

export const Facilidad = sequelize.define('Facilidad', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    nombreFacilidad: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
    tableName: "Facilidades",
    freezeTableName: true
});

