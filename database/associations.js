//ESTE ARCHIVO NO SE USARA, LAS RELACIONES ESTAN HECHAS
//EN LOS MODELOS 

import { Ambiente } from "../models/Ambiente.model.js";
import { Facilidad } from "../models/Facilidad.model.js";
import { Tipo } from "../models/Tipo.model.js";
import { FacilidadAmbiente } from "../models/FacilidadAmbiente.model.js";
import { Docente } from "../models/Docente.model.js";
import { Periodo } from "../models/Periodo.model.js";
import { Reserva } from "../models/Reserva.model.js";

// Muchos ambiente puede tener muchas facilidades
// Muchas facilidades pueden pertenecer a muchos ambientes

// Ambiente.belongsToMany(Facilidad, {
//   through: FacilidadAmbiente,
//   as: "Facilidades",
// });
// Facilidad.belongsToMany(Ambiente, {
//   through: FacilidadAmbiente,
//   as: "Facilidades",
// });

// Muchos ambientes pueden ser de un tipo
// Un tipo de ambiente puede pertenecer a muchos ambientes

// Tipo.hasMany(Ambiente,{
//   foreignKey: 'tipoId',
//   sourceKey: 'id'
// });
// Ambiente.belongsTo(Tipo,{
//   foreignKey: 'tipoId',
//   targetId: 'id'
// });

// Reserva.belongsTo(Periodo, {
//   as: "periodoIni",
// });
// Reserva.belongsTo(Periodo, {
//   as: "periodoFin",
// });
// Periodo.hasMany(Reserva);

// Reserva.belongsTo(Docente);
// Docente.hasMany(Reserva);
