import { Ambiente } from "../models/Ambiente.model.js";
import { Facilidad } from "../models/Facilidad.model.js";
import { FacilidadAmbiente } from "../models/FacilidadAmbiente.model.js";
import { Tipo } from "../models/Tipo.model.js";
import { Periodo } from "../models/Periodo.model.js";
import { Docente } from "../models/Docente.model.js";
import { Reserva } from "../models/Reserva.model.js";

//ACA SE CREARA LOS DATOS POR DEFECTO QUE LA BASE DE DATOS YA DEBEIRA TENER

export async function seedData() {
  await Tipo.bulkCreate([
    { nombreTipo: "Clase" },
    { nombreTipo: "Auditorio" },
    { nombreTipo: "Laboratorio" },
  ]);
  await Ambiente.bulkCreate([
    {
      nombreAmbiente: "MEMI 1",
      descripcion: "primera aula de edificio MEMI",
      capacidad: 30,
      activo: true,
      disponibilidad: true,
      tipoId: 3,
    },
    {
      nombreAmbiente: "691A",
      descripcion: "aula mediana en 2do piso",
      capacidad: 100,
      activo: true,
      disponibilidad: true,
      tipoId: 1,
    },
    {
      nombreAmbiente: "690A",
      descripcion: "aula de planta baja, pequenha",
      capacidad: 15,
      activo: "true",
      disponibilidad: true,
      tipoId: 1,
    },
    {
      nombreAmbiente: "650",
      descripcion: "aula pequenha",
      capacidad: 25,
      activo: true,
      disponibilidad: true,
      tipoId: 1,
    },
    {
      nombreAmbiente: "651",
      descripcion: "aula mediana",
      capacidad: 40,
      activo: true,
      disponibilidad: true,
      tipoId: 1,
    },
    {
      nombreAmbiente: "690A",
      descripcion: "aula grande",
      capacidad: 80,
      activo: true,
      disponibilidad: true,
      tipoId: 1,
    },
    {
      nombreAmbiente: "Auditorio",
      descripcion: "auditorio de examenes",
      capacidad: 150,
      activo: true,
      disponibilidad: true,
      tipoId: 2,
    },
  ]);
  await Facilidad.bulkCreate([
    { nombreFacilidad: "Equipos" },
    { nombreFacilidad: "Proyector" },
    { nombreFacilidad: "Computadora" }
  ]);
  await FacilidadAmbiente.bulkCreate([
    {ambienteId:1 ,facilidadId:1},
    {ambienteId:1 ,facilidadId:2},
    {ambienteId:1 ,facilidadId:3},
    {ambienteId:2 ,facilidadId:2},
    {ambienteId:2 ,facilidadId:3},
    {ambienteId:3 ,facilidadId:2},
    {ambienteId:3 ,facilidadId:3},
    {ambienteId:4 ,facilidadId:2},
    {ambienteId:4 ,facilidadId:3},
    {ambienteId:5 ,facilidadId:1},
    {ambienteId:5 ,facilidadId:2},
    {ambienteId:5 ,facilidadId:3},
    {ambienteId:6 ,facilidadId:1},
    {ambienteId:6 ,facilidadId:2},
    {ambienteId:6 ,facilidadId:3},
    {ambienteId:7 ,facilidadId:1},
    {ambienteId:7 ,facilidadId:2},
    {ambienteId:7 ,facilidadId:3}
  ]);
  await Periodo.bulkCreate([
    { hora: "06:45" },
    { hora: "08:15" },
    { hora: "09:45" },
    { hora: "11:15" },
    { hora: "12:45" },
    { hora: "14:15" },
    { hora: "15:45" },
    { hora: "17:15" },
    { hora: "18:45" },
    { hora: "20:15" },
    { hora: "21:45" },
  ]);
  await Docente.bulkCreate([
    { nombre: "Juan", apellido: "Perez", edad: 45 },
    { nombre: "Alfredo", apellido: "Zeballos", edad: 50 },
    { nombre: "Maria", apellido: "Fernandez", edad: 49 },
  ]);
}
