import { Periodo } from '../models/Periodo.model.js';


export const getPeriodos = async (req,res)=>{
  try{
    const periodos = await Periodo.findAll();
    res.json(periodos);
  }catch(error){
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
};

