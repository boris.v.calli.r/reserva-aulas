import { Tipo } from '../models/Tipo.model.js';

export const getTipos = async(req,res)=>{
  try{
    const tipos = await Tipo.findAll();
    res.json(tipos);
  }catch(error){
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
}