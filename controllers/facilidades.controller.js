import { Facilidad } from '../models/Facilidad.model.js';

export const getFacilidades = async(req,res)=>{
  try{
    const tipos = await Facilidad.findAll();
    res.json(tipos);
  }catch(error){
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
}