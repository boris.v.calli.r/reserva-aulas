import { Ambiente } from "../models/Ambiente.model.js";
import { Docente } from "../models/Docente.model.js";
import { Periodo } from "../models/Periodo.model.js";
import { Reserva } from "../models/Reserva.model.js";
import { sePuedeReservar } from "../utils/reserva.js";


export const createReserva = async (req, res) => {
  const { docente, materia, razon, ambiente, fecha, horaInicio, horaFin } =
    req.body;
  try {

    let horasReservadas = (await Reserva.findAll({
      attributes: ['fecha'],
      where: {
        fecha: fecha
      },
      include: [{
        model: Periodo,
        as: 'periodoIni',
        attributes: ['hora']
      }, {
        model: Periodo,
        as: 'periodoFin',
        attributes: ['hora']
      }
      ]
    })).map(item => {
      return { ini: item.periodoIni.hora, fin: item.periodoFin.hora };
    });

    let sePuede = true;
    horasReservadas.forEach(hora => {
      sePuede &= sePuedeReservar(hora.ini, hora.fin, horaInicio, horaFin);
    })
    if (!sePuede) {
      return res.status(200).json({
        "message": "No se puede reservar, hay conflito con otros horarios"
      })
    }




    let docenteBusc = await Docente.findOne({
      where: {
        nombre: docente.split(" ")[0],
      },
    });
    if (docenteBusc == null) {
      res.status(404).json({
        message: "El docente especificado no esta registrado en el sistema",
      });
    }
    let docenteBuscId = docenteBusc.id;
    let ambienteBusc = await Ambiente.findOne({
      where: {
        nombreAmbiente: ambiente,
      },
    });
    if (ambienteBusc == null) {
      res.status(404).json({
        message: "El ambiente especificado no esta registrado en el sistema",
      });
    }
    let ambienteBuscId = ambienteBusc.id;
    let horaIniBusc = await Periodo.findOne({
      where: {
        hora: horaInicio,
      },
    });
    let horaFinBusc = await Periodo.findOne({
      where: {
        hora: horaFin,
      },
    });
    let horaIniBuscId = horaIniBusc.id;
    let horaFinBuscId = horaFinBusc.id;
    await Reserva.create({
      materia: materia,
      razon: razon,
      fecha: fecha,
      docenteId: docenteBuscId,
      periodoIniId: horaIniBuscId,
      periodoFinId: horaFinBuscId,
      ambienteId: ambienteBuscId,
    });
    res.sendStatus(204);

    // res.status(200).json({
    //   "message": "Se creo una reserva"
    // })


    // } else {
    // res.status(400).json({
    //   "message":"No se puede realizar la reserva"
    // })
    // }
  } catch (error) {
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
};

export const getListaReserva = async (req, res) => { };
