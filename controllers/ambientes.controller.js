import { Ambiente } from "../models/Ambiente.model.js";
import { FacilidadAmbiente } from "../models/FacilidadAmbiente.model.js";
import { Facilidad } from "../models/Facilidad.model.js";
import { Tipo } from "../models/Tipo.model.js";
import { Periodo } from "../models/Periodo.model.js";

export const getListaAmbiente = async (req, res) => {
  try {
    const ambientes = await Ambiente.findAll({
      include: [
        {
          model: Tipo,
        },
        {
          model: Facilidad,
        },
      ],
    });
    let ambs = ambientes.map((model) => model.toJSON());
    ambs = ambs.map((amb) => {
      let nuevoAmb = { ...amb };
      delete nuevoAmb.Tipo;
      delete nuevoAmb.tipoId;
      nuevoAmb.nombreTipo = amb.Tipo.nombreTipo;

      let facilidades = [];
      for (let fac of amb.Facilidads) {
        facilidades.push(fac.nombreFacilidad);
      }
      delete nuevoAmb.Facilidads;
      nuevoAmb.facilidades = facilidades;
      return nuevoAmb;
    });
    res.json(ambs);
  } catch (error) {
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
};
export const getAmbiente = async (req, res) => {
  try {
    const ambienteId = parseInt(req.params.id);
    const ambiente = await Ambiente.findByPk(ambienteId);
    if (ambiente == null) {
      res.status(404).json({
        message: "no se encuentra el ambiente especificado",
      });
    } else {
      res.json(ambiente);
    }
  } catch (error) {
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
};
export const createAmbiente = async (req, res) => {
  try {
    const {
      nombreAmbiente,
      descripcion,
      capacidad,
      disponibilidad,
      facilidades,
      tipoAmbiente,
    } = req.body;
    const nuevoAmb = new Ambiente();
    nuevoAmb.nombreAmbiente = nombreAmbiente;
    nuevoAmb.descripcion = descripcion;
    nuevoAmb.capacidad = capacidad;
    nuevoAmb.disponibilidad = disponibilidad;
    const tipoBusc = await Tipo.findOne({
      where: {
        nombreTipo: tipoAmbiente,
      },
    });
    let tipoId = tipoBusc.id;
    nuevoAmb.tipoId = tipoId;
    await nuevoAmb.save();
    let nuevoAmbienteId = nuevoAmb.id;

    //verificamos que facilidades no es arreglo
    //el form manda un valor en lugar de arreglo si se selecciona un elemento
    let facilidadesArr;
    if (typeof facilidades != Array) {
      facilidadesArr = [facilidades]
    }
    for (let facilidad of facilidadesArr) {
      let facilidadBusc = await Facilidad.findOne({
        where: {
          nombreFacilidad: facilidad,
        },
      });
      await FacilidadAmbiente.create({
        facilidadId: facilidadBusc.id,
        ambienteId: nuevoAmbienteId,
      });
    }
    res.status(200).json({
      "message": "bien hecho"
    });
  } catch (error) {
    res.status(500).json({
      message: `Error: ${error} daaaa`,
    });
  }
};
export const createAmbienteLote = async (req, res) => {
  console.log(req.body);
  try {
    res.json(req.body);
  } catch (error) {
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
  // try {
  //   const ambientes = req.body;
  //   //primero creamos ambientes por lote
  //   let nuevosAmbientes = [];
  //   for (const ambiente of ambientes) {
  //     const {
  //       nombreAmbiente,
  //       descripcion,
  //       capacidad,
  //       disponibilidad,
  //       facilidades,
  //       tipoAmbiente,
  //     } = ambiente;
  //     const nuevoAmb = new Ambiente();
  //     nuevoAmb.nombreAmbiente = nombreAmbiente;
  //     nuevoAmb.descripcion = descripcion;
  //     nuevoAmb.capacidad = capacidad;
  //     nuevoAmb.disponibilidad = disponibilidad;
  //     let tipoId = (
  //       await Tipo.findOne({
  //         where: {
  //           nombreTipo: tipoAmbiente,
  //         },
  //       })
  //     ).id;
  //     nuevoAmb.tipoId = tipoId;
  //     await nuevoAmb.save();
  //     for (const facilidad of facilidades) {
  //       let facilidadBusc = await Facilidad.findOne({
  //         where:{
  //           nombreFacilidad:facilidad
  //         }
  //       })
  //       let facilidadBuscId = facilidadBusc.id;
  //       await FacilidadAmbiente.create({"ambienteId":nuevoAmb.id,"facilidadId":facilidadBuscId});
  //     }
  //   }

  //   res.sendStatus(204);
  // } catch (error) {
  //   res.status(500).json({
  //     message: `Error: ${error}`,
  //   });
  // }
};
export const deleteAmbiente = async (req, res) => {
  try {
    const ambienteId = req.params.id;
    const ambiente = await Ambiente.findByPk(ambienteId);
    if (ambiente == null) {
      res.status(404).json({
        message: "no se encuentra el ambiente especificado para eliminar",
      });
    } else {
      await ambiente.update(
        { activo: false },
        {
          where: {
            id: ambienteId,
          },
        }
      );
      res.json(ambiente);

      //otra manera de actualizar activo a false
      //   ambiente.activo = false;
      //   await ambiente.save();
      //   res.json(ambiente);
    }
  } catch (error) {
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
};
export const activarAmbiente = async (req, res) => {
  try {
    const ambienteId = req.params.id;
    const ambiente = await Ambiente.findByPk(ambienteId);
    if (ambiente == null) {
      res.status(404).json({
        message: "no se encuentra el ambiente especificado para eliminar",
      });
    } else {
      await ambiente.update(
        { activo: true },
        {
          where: {
            id: ambienteId,
          },
        }
      );
      res.json(ambiente);

      //otra manera de actualizar activo a false
      //   ambiente.activo = false;
      //   await ambiente.save();
      //   res.json(ambiente);
    }
  } catch (error) {
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
}

export const reservarAmbiente = async (req, res) => {
  try {
    let tipos = await Tipo.findAll();
    let horas = await Periodo.findAll();
    let ambientes = await Ambiente.findAll({
      attributes: ["id", "nombreAmbiente", "capacidad", "activo"],
      where: {
        activo: true
      }
    });
    tipos = tipos.map(tipo => {
      return tipo.toJSON();
    })
    horas = horas.map(hora => {
      return hora.toJSON();
    })
    ambientes = ambientes.map(amb => {
      return amb.toJSON();
    })
    let horasArr = [];
    let tiposArr = [];
    let ambsArr = [];

    tipos.forEach(tipo => {
      tiposArr.push(tipo.nombreTipo);
    })
    horas.forEach(hora => {
      horasArr.push(hora.hora);
    })
    ambientes.forEach(amb => {
      ambsArr.push({
        "ambiente": amb.nombreAmbiente,
        "capacidad": amb.capacidad
      });
    })

    res.render("reservarAmbiente", {
      "horas": horasArr,
      "tipos": tiposArr,
      "ambientes": ambsArr
    });
  } catch (error) {
    console.error("Error en la vista");
    res.render("error en la vista");
  }
};
export const crearAmbienteVista = async (req, res) => {
  let tipos = await Tipo.findAll();
  let facilidades = await Facilidad.findAll();

  tipos = tipos.map(tipo => {
    return tipo.toJSON();
  });
  facilidades = facilidades.map(facilidad => {
    return facilidad.toJSON();
  });

  let tiposArr = [];
  let facilidadesArr = [];

  tipos.forEach(tipo => {
    tiposArr.push(tipo.nombreTipo);
  });
  facilidades.forEach(fac => {
    facilidadesArr.push(fac.nombreFacilidad);
  });

  res.render('crearAmbiente', {
    "tipos": tiposArr,
    "facilidades": facilidadesArr
  });
}
export const getAmbientes = async (req, res) => {
  try {
    const ambientes = await Ambiente.findAll({
      include: [
        {
          model: Tipo,
        },
        {
          model: Facilidad,
        },
      ],
    });
    let ambs = ambientes.map((model) => model.toJSON());
    ambs = ambs.map((amb) => {
      let nuevoAmb = { ...amb };
      delete nuevoAmb.Tipo;
      delete nuevoAmb.tipoId;
      nuevoAmb.nombreTipo = amb.Tipo.nombreTipo;

      let facilidades = [];
      for (let fac of amb.Facilidads) {
        facilidades.push(fac.nombreFacilidad);
      }
      delete nuevoAmb.Facilidads;
      nuevoAmb.facilidades = facilidades;
      return nuevoAmb;
    });
    res.render("listaAmbientes", {
      ambientes: ambs,
    });
  } catch (error) {
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
};
export const eliminarAmbienteVista = async (req, res) => {
  try {
    const ambientes = await Ambiente.findAll({
      include: [
        {
          model: Tipo,
        }
      ],
    });
    let ambs = ambientes.map((model) => model.toJSON());
    ambs = ambs.map((amb) => {
      let nuevoAmb = { ...amb };
      delete nuevoAmb.Tipo;
      delete nuevoAmb.tipoId;
      nuevoAmb.nombreTipo = amb.Tipo.nombreTipo;
      return nuevoAmb;
    });
    res.render('eliminarAmbiente', {
      ambientes: ambs
    });

  } catch (error) {
    res.status(500).json({
      message: `Error: ${error}`,
    });
  }
}

export const getAmbientesPaginados = async (req, res) => {
  const resultsPerPage = 5; // Cantidad de ambientes por página
  const page = req.query.page ? parseInt(req.query.page, 10) : 1;
  const offset = (page - 1) * resultsPerPage;
  try {
    const ambientes = await Ambiente.findAll({
      offset: offset,
      limit: resultsPerPage,
      include: [
        {
          model: Tipo,
        },
        {
          model: Facilidad,
        },
      ],
    });
    const totalAmbientes = await Ambiente.count();
    const numberOfPages = Math.ceil(totalAmbientes / resultsPerPage);

    res.json({ ambientes, numberOfPages });

  } catch (error) {
    console.error('Error al obtener ambientes:', error);
    res.status(500).json({ error: 'Error en el servidor' });

  }
}

export const getAmbientesPorPagina = async (req, res) => {
  const page = req.query.page ? parseInt(req.query.page, 10) : 1;

  try {
    const dataFetched = await fetch(`http://localhost:3000/api/ambientes-paginados?page=${page}`);

    if (!dataFetched.ok) {
      throw new Error('La respuesta de la network no fue correcta');
    }

    const jsonData = await dataFetched.json();

    res.render('listaAmbientes', { ambientes: jsonData.ambientes, page, numberOfPages: jsonData.numberOfPages });
  } catch (error) {
    console.error('Fetch error:', error);
    res.render('error', { error: 'Error en la vista' }); // Render the "error" view
  }
}
