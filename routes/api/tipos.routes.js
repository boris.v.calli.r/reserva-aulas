import { Router } from 'express';
import { getTipos } from '../../controllers/tipos.controller.js';

export const tipoRouter = Router();

tipoRouter.get('/api/tipo',getTipos);