import { Router } from 'express';
import {
  getListaAmbiente,
  getAmbiente,
  createAmbiente,
  deleteAmbiente,
  createAmbienteLote,
  getAmbientes,
  reservarAmbiente,
  crearAmbienteVista,
  eliminarAmbienteVista,
  getAmbientesPaginados,
  getAmbientesPorPagina
} from "../../controllers/ambientes.controller.js";


export const ambienteRouter = Router();


//vistas
ambienteRouter.get('/crear-ambiente', crearAmbienteVista);
//ambienteRouter.get('/lista-ambientes', getAmbientes);
ambienteRouter.get('/lista-ambientes', getAmbientesPorPagina);
ambienteRouter.get('/reservar-ambiente',reservarAmbiente)
ambienteRouter.get('/eliminar-ambiente',eliminarAmbienteVista);

//api
// ambienteRouter.post('/crear-ambiente', crearAmbiente);


//--------------------------------------------------------------------------------
ambienteRouter.get('/api/ambiente',getListaAmbiente);
ambienteRouter.get('/api/ambientes-paginados', getAmbientesPaginados);
ambienteRouter.get('/api/ambiente/:id',getAmbiente);
ambienteRouter.post('/api/ambiente',createAmbiente);
ambienteRouter.delete('/api/ambiente/:id',deleteAmbiente)
ambienteRouter.post('/api/ambiente-lote',createAmbienteLote)