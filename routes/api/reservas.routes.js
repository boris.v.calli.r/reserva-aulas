import { Router } from "express";
import {
  getListaReserva,
  createReserva,
} from "../../controllers/reservas.controller.js";

export const reservaRouter = Router();

reservaRouter.get("/api/reserva", getListaReserva);
reservaRouter.post("/api/reserva", createReserva);
