import { Router } from 'express';
import { getFacilidades } from '../../controllers/facilidades.controller.js';

export const facilidadRouter = Router();

facilidadRouter.get('/api/facilidad',getFacilidades);