import { Router } from 'express';
import { getPeriodos } from '../../controllers/periodos.controllers.js';

export const periodoRouter = Router();

periodoRouter.get('/api/periodo',getPeriodos);
