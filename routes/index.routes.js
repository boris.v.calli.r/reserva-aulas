import express from 'express';
import { sequelize } from '../database/db.js';

export const indexRouter = express.Router();

indexRouter.get('/', async (req, res) => {
    
    res.render('index');
});


