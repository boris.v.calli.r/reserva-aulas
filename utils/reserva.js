export const sePuedeReservar = (horaIni, horaFin, reservaIni, reservaFin) => {
  let horaIniSec = convertirHoraASegundos(horaIni);
  let horaFinSec = convertirHoraASegundos(horaFin);
  let reservaIniSec = convertirHoraASegundos(reservaIni);
  let reservaFinSec = convertirHoraASegundos(reservaFin);
  if (reservaIniSec >= horaFinSec) {
    return true;
  } else if (reservaFinSec <= horaIniSec) {
    return true;
  } else {
    return false;
  }
};



const convertirHoraASegundos = (horaStr) => {
  let datos = horaStr.split(":");
  let hora = parseInt(datos[0]);
  let minutos = parseInt(datos[1]);
  let segundos = 3600 * hora + minutos * 60;
  return segundos;
};

