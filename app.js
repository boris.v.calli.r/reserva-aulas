import path from 'path';
import { fileURLToPath } from 'url';
import express from 'express';

import { indexRouter } from './routes/index.routes.js';
import { ambienteRouter } from './routes/api/ambiente.routes.js';
import { periodoRouter } from './routes/api/periodos.routes.js';
import { tipoRouter } from './routes/api/tipos.routes.js';
import { facilidadRouter } from './routes/api/facilidades.routes.js';
import { reservaRouter } from './routes/api/reservas.routes.js';
import { sequelize } from './database/db.js';
import './database/associations.js';
import { seedData } from './database/seeds.js';

const app = express();

app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
app.use(express.static(__dirname + '/public'));

const PORT = 3000;


app.use(tipoRouter);
app.use(indexRouter);
app.use(periodoRouter);
app.use(ambienteRouter);
app.use(facilidadRouter);
app.use(reservaRouter);

async function main() {
    try {
        await sequelize.authenticate();
        await sequelize.sync({ force: true });
        // await asociarTablas();
        app.listen(PORT);
        console.log('Connection has been established successfully.');
        seedData()

    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

main();