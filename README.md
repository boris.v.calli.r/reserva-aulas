# Proyecto de reserva de aulas
## Funcionalidades
- Lista de aulas.
- Reserva de aula.
- Aulas reservadas.
- Creacion de aulas.
- Eliminacion de aulas.

## Explicacion de las funcionalidades
### Lista de aulas
En la seccion de la barra de navegacion se tiene lo que seria la opcion de ver la lista de aulas una vez clickamos la opcion nos mostrara un listado de las aulas donde estaran paginadas de 5 en 5 o sea cada pagina tendra 5 filas de aulas. Dichas aulas tienen la siguiente informacion: nombre, capacidad, descripcion, tipo, facilidades y si esta activo.
### Reserva de aula
La reserva del aula es un formulario donde los docentes pueden hacer la reserva de las aulas basandose en la lista de aulas ya vista puesto que en dicha lista se mostraban caracteristicas de estas y el docente puede buscar la que mas le convenga.
Dicho formulario contiene los siguientes campos:
- **Docente:** En dicho campo el docente debe escribir su nombre completo.
- **Materia:** Se debe de escribir la materia para la que se quiere hacer la reserva.
- **Razon de reserva:** Aqui se debe de justificiar el motivo de la reserva del aula.
- **Aula:** Aqui el docente puede escoger que aula quiere reservar cuando escoja una mostrara en una etiqueta debajo lo que es la capacidad y asi ver si es lo que necesita.
- **Capacidad minima:** Este campo sera un filtro en caso de querer de forma especiifica una cantidad de espacio especifica en el aula, una vez escrita la cantidad y filtrar la seccion donde se escoje las aulas estara ahora en base a la cantidad que se quiere que tenga.
- **Fecha:** Aqui el docente debe escribir la fecha que se esta haciendo la reserva.
- **Hora inicio:** Aqui el docente debe escoger la hora de inicio de su clase para el aula que quiere reservar.
- **Hora final:** Aqui el docente debe escoger la hora de finalizacion o culminacion de su clase para el aula que quiere reservar.
- **Reservar:** Es el boton para efectuar la reserva una vez que los campos esten llenos.

### Creacion de aulas
La seccion de creacion de aulas tiene un formulario para creacion de aulas que tiene los siguientes campos:
- **Nombre del ambiente:** Aqui se debe de escribir el nombre del aula.
- **Descripcion:** En este campo se debe hacer una breve descripcion del aula denotando alguna o algunas caracteristicas para esta.
- **Capacidad:** Se debe de escribir la capacidad en numero entero que tiene el aula.
- **Facilidad:** Se debe designar que facilidad o facilidades poseera la aula.
- **Tipo de ambiente:** Hace referencia a que si es un aula unicamente o es un laboratorio o tambien un auditorio inclusive puede ser multifuncional. Puede cambiar con el tiempo agregando mas opciones.

Tambien aparte del formulario se tiene un apartado donde se puede hacer un cargado de datos a la base de datos mediante un archivo de tipo 'csv'.
### Aulas reservadas
No se ha concluido dicha vista.
### Eliminacion de aulas
En esta seccion se tienen dos listas donde se pueden activar y desactivar aula no necesariamente se eliminan de la base de datos sino que pasan a un estado donde las aulas que estan inactivas o desactivadas o sea no podran reservarse.

Para dicha accion se tiene un boton 'activar' para que el aula pueda ser reservada nuevamente y un boton de 'desactivar' para que el aula pase a un estado donde no se puede reservar.